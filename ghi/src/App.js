import CocktailDetails from './CocktailDetails';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import "./App.css";
import SignUp from "./SignUp.js";
import Login from "./Login";
import Nav from './Nav';
import ProfileTemplate from './ProfileTemplate';
import CocktailForm from "./CocktailForm";
import SearchPage from './SearchPage';
import Home from './Home';
import UpdateCocktailForm from './UpdateCocktailForm';
import Footer from './Footer';




function App() {

  return (
    <BrowserRouter>
      <Nav />
      <div>
        <Routes>
          <Route path="/" >
            <Route path="" element={<Home />} />
            <Route path="login" element={<Login />} />
            <Route path="signup" element={<SignUp />} />
            <Route path="search" element={<SearchPage />} />
          </Route>
          <Route path="/cocktails" >
            <Route path="" element={<SearchPage />} />
            <Route path="new" element={<CocktailForm />} />
            <Route path=":cocktail_id" element={<CocktailDetails />} />
            <Route path="update/:cocktail_id" element={<UpdateCocktailForm />} />
          </Route>
          <Route path="/profile">
            <Route path="" element={<ProfileTemplate />} />
          </Route>
        </Routes>
        <Footer />
      </div>
    </BrowserRouter>
  );
}

export default App;
