from queries.client import MongoQueries
from models.favorites import FavoriteIn, FavoriteOut
from bson.objectid import ObjectId


class FavoritesQueries(MongoQueries):
    collection_name = "favorites"

    def create_favorite(self, account_id: str, favorite_in: FavoriteIn):
        favorite_data = favorite_in.dict()
        favorite_data["account_id"] = str(account_id)

        result = self.collection.insert_one(favorite_data)
        if result is None:
            return None

        favorite_data["id"] = str(result.inserted_id)
        return FavoriteOut(**favorite_data)

    def get_all_favorites(self, account_id: str):
        result = []
        favorite_db = list(self.collection.find({"account_id": account_id}))
        for favorite in favorite_db:
            favorite["id"] = str(favorite["_id"])
            del favorite["_id"]
            result.append(dict(favorite))
        return result

    def delete(self, id: str, account_id: str):
        result = self.collection.delete_one(
            {"_id": ObjectId(id), "account_id": account_id}
        )
        return result.deleted_count > 0
