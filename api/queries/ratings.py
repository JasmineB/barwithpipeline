from queries.client import MongoQueries
from pymongo import ReturnDocument
from models.ratings import (
    RatingIn,
    RatingOut,
)


class RatingsQueries(MongoQueries):
    collection_name = "ratings"

    def get_cocktail_ratings(self, cocktail_id: str):
        ratings_db = list(self.collection.find({"cocktail_id": cocktail_id}))
        ratings_list = []
        for rating in ratings_db:
            rating["id"] = str(rating["_id"])
            del rating["_id"]
            ratings_list.append(dict(rating))
        return ratings_list

    def get_avg_cocktail_ratings(self, cocktail_id: str):
        cocktail_ratings_db = self.collection.aggregate(
            [
                {"$match": {"cocktail_id": cocktail_id}},
                {
                    "$group": {
                        "_id": "$cocktail_id",
                        "ratings_average": {"$avg": "$rating"},
                    }
                },
            ]
        )
        cocktail_ratings = list(cocktail_ratings_db)
        if cocktail_ratings:
            return cocktail_ratings[0]["ratings_average"]
        else:
            return 0

    def get_cocktail_ratings_count(self, cocktail_id: str):
        cocktail_ratings_db = self.collection.aggregate(
            [{"$match": {"cocktail_id": cocktail_id}},
             {"$count": "ratings_count"}]
        )
        cocktail_ratings = list(cocktail_ratings_db)
        if cocktail_ratings:
            return cocktail_ratings[0]["ratings_count"]
        else:
            return 0

    def get_user_rating(self, cocktail_id: str, account_id: str):
        user_rating = self.collection.find_one(
            {"cocktail_id": cocktail_id, "account_id": account_id}
        )
        if user_rating is None:
            return None
        user_rating["id"] = str(user_rating["_id"])
        del user_rating["_id"]
        return RatingOut(**user_rating)

    def update_or_create_user_rating(
        self, cocktail_id: str,
        account_name: str,
        account_id: str,
        rating_in: RatingIn
    ):
        updated_rating = self.collection.find_one_and_update(
            {
                "cocktail_id": cocktail_id,
                "account_name": account_name,
                "account_id": account_id,
            },
            {"$set": {**rating_in.dict()}},
            return_document=ReturnDocument.AFTER,
        )

        if updated_rating is None:
            user_rating = rating_in.dict()
            user_rating["cocktail_id"] = cocktail_id
            user_rating["account_name"] = account_name
            user_rating["account_id"] = account_id

            new_rating = self.collection.insert_one(user_rating)
            if new_rating is None:
                return None
            user_rating["id"] = str(new_rating.inserted_id)
            return RatingOut(**user_rating)
        else:
            updated_rating["id"] = str(updated_rating["_id"])
            del updated_rating["_id"]
            return RatingOut(**updated_rating)
