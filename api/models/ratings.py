from pydantic import BaseModel
from typing import List


class RatingIn(BaseModel):
    review: str
    rating: int


class RatingOut(RatingIn):
    id: str
    account_name: str
    account_id: str
    cocktail_id: str


class RatingsList(BaseModel):
    ratings: List[RatingOut]


class RatingAverage(BaseModel):
    ratings_average: float


class RatingsCount(BaseModel):
    ratings_count: int
