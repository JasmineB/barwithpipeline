from pydantic import BaseModel
from enum import Enum
from typing import List


class CocktailCatagory(str, Enum):
    ordinary_drink = ("ordinary drink",)
    cocktail = ("cocktail",)
    shake = ("shake",)
    shot = ("shot",)
    coffee_tea = ("coffee or tea",)
    other = ("other",)


class CocktailIn(BaseModel):
    cocktail_name: str
    alcoholic: bool
    ingredients: str
    image_url: str
    instructions: str
    suggested_serving_glass: str
    category: CocktailCatagory = "Pick a cocktail"


class CocktailOut(CocktailIn):
    id: str
    account_id: str


class CocktailList(BaseModel):
    cocktails: List[CocktailOut]


class DeleteStatus(BaseModel):
    success: bool
