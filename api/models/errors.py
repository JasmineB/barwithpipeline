from pydantic import BaseModel


class DuplicateAccountError(ValueError):
    pass


class HttpError(BaseModel):
    detail: str


class Error(BaseModel):
    detail: str
