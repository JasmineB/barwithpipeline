fastapi[all]==0.81.0
uvicorn[standard]==0.17.6
jwtdown-fastapi>=0.2.0
pytest
pymongo==4.3.3
