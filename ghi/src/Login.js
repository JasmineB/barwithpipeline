import React, { useState } from "react";
import { useLoginMutation } from "./app/cocktailSlice";
import {useNavigate} from "react-router-dom"


export const Login = () => {
    const [login] = useLoginMutation();
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');
    const navigate = useNavigate()

    const handleSubmit = async(e) => {
        e.preventDefault();
        login({username, password});
        navigate("/")
    }

    const handleUsernameChange = (e) => {
        const value = e.target.value;
        setUsername(value);
    }

    const handlePasswordChange = (event) => {
      const value = event.target.value;
      setPassword(value);
    };

    const handleClick = (e) => {
      navigate("/signup")
    }

    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
      <div className="container p-5 my-5 border row">
      <div className="auth-form-container col">
        <form id="1" onSubmit={handleSubmit}>
          <h1>Login</h1>
          <p></p>
          <label htmlFor="username">Email</label><p>
          <input
            value={username}
            onChange={handleUsernameChange}
            type="username"
            placeholder="Username/Email"
            id="username"
            name="username"
          />
          </p>
          <p></p>

          <label htmlFor="password">Password</label><p>
          <input
            value={password}
            onChange={handlePasswordChange}
            type="password"
            placeholder="********"
            id="password"
            name="password"
          /></p>

<button form="1">Log In </button>
        </form>
        </div>

        <div className="container border-start border-dark col text-center ">
          <p className="mb-5"></p>
          <p className="mb-5"></p>
          <h1>- or -</h1>
          <p className="mb-3" ></p>

          <button onClick={handleClick}>Sign Up</button>
        </div>

      </div>
      </div>
      </div>
      </div>
    );
}
export default Login;
