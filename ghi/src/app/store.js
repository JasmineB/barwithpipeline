import { configureStore } from "@reduxjs/toolkit";
import { accountsApi } from "./cocktailSlice";
import { getDefaultMiddleware } from "@reduxjs/toolkit";


export default configureStore({
  reducer: {
    [accountsApi.reducerPath]: accountsApi.reducer,
  },
  middleware: (getDefaultmiddleware) => getDefaultMiddleware()
    .concat(accountsApi.middleware),
});
