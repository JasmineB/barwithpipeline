import React, { useEffect } from 'react';
import { useCreateFavoritesMutation, useDeleteFavoriteMutation, useListFavoritesQuery, useGetAccountQuery } from './app/cocktailSlice';
import { ReactComponent as HeartIcon } from './images/heart.svg';
import { ReactComponent as HeartFilledIcon } from './images/heartfilled.svg';


const FavoriteToggleButton = ({ cocktail }) => {
    const [createFavorite] = useCreateFavoritesMutation();
    const [getDelete] = useDeleteFavoriteMutation();
    const { data: favorites, refetch } = useListFavoritesQuery();
    const { data: account } = useGetAccountQuery();
    const accountId = account && account.account ? account.account.id : null;
    const favorite = favorites ? favorites.favorites.find(fav => fav.cocktail_name === cocktail.cocktail_name && fav.account_id === accountId) : null;


    useEffect(() => {
        refetch();
    }, [cocktail, refetch]);

    const handleToggle = async (event) => {
        event.preventDefault();
        if (accountId) {


            if (favorite && favorite.cocktail_name) {
                try {
                    await getDelete(favorite.id);
                    refetch();
                } catch (error) {
                    console.log('Error deleting a favorite!:', error);
                }
            } else {
                try {

                    await createFavorite({ cocktail_name: cocktail.cocktail_name });
                    console.log();
                    refetch();
                } catch (error) {
                    console.log('Error adding favorite:', error);
                }
            }
        } else {
            console.log('User is not logged in.');
        }
    };

    return (
        <div className={favorite ? "favoriteIcon active" : "favoriteIcon"} onClick={handleToggle}>
            {favorite ? <HeartFilledIcon className="heartSvg" /> : <HeartIcon className="heartSvg" />}
        </div>
    );
};

export default FavoriteToggleButton;
