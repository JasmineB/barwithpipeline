import { Link } from "react-router-dom";
import bartender from "./images/bartender-home.jpeg";
import drinks from "./images/drinks-home.jpeg";
import { useGetAccountQuery } from "./app/cocktailSlice";

function Home() {
	const { data: account } = useGetAccountQuery();

	return (
		<div className="px-4 py-5 my-5 text-center">
			<center>
				<h1 className="title-container display-5 fw-bold">Be the Bartender of your dreams!</h1>
			</center>
			<div className="container" style={{ marginTop: "50px" }}>
				<div className="row">
					<div className="col">
						<Link to="/search">
							<img src={drinks} className="big-img" alt="Explore Cocktails!" />
						</Link>
					</div>
					<div className="col">
						<Link to={(account && "/profile") || "/login"}>
							<img src={bartender} className="big-img" alt="Make Your own Cocktails!" />
						</Link>
					</div>
				</div>
			</div>
		</div>
	);
}

export default Home;
