import React, { useState } from 'react';
import { useCocktailsNewMutation } from './app/cocktailSlice';
import { useNavigate } from "react-router-dom";

function CocktailForm() {
  const navigate = useNavigate();
  const [cocktailName, setCocktailName] = useState('');
  const [alcoholic, setAlcoholic] = useState();
  const [ingredients, setIngredients] = useState('');
  const [image, setImage] = useState('');
  const [instructions, setInstructions] = useState('');
  const [servingGlass, setServingGlass] = useState('');
  const [category, setCategory] = useState('');

  const [createCocktail] = useCocktailsNewMutation();

  const handleCocktailNameChange = (event) => {
    const value = event.target.value;
    setCocktailName(value);
  };

  const handleAlcoholicChange = (event) => {
    const value = event.target.value;
    setAlcoholic(value);
  };

  const handleIngredientChange = (event) => {
    const value = event.target.value;
    setIngredients(value);
  };

  const handleImageChange = (event) => {
    const value = event.target.value;
    setImage(value);
  };

  const handleInstructionChange = (event) => {
    const value = event.target.value;
    setInstructions(value);
  };

  const handleServingGlassChange = (event) => {
    const value = event.target.value;
    setServingGlass(value);
  };

  const handleCategoryChange = (event) => {
    const value = event.target.value;
    setCategory(value);
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    createCocktail({
      cocktail_name: cocktailName,
      alcoholic: alcoholic,
      ingredients: ingredients,
      image_url: image,
      instructions: instructions,
      suggested_serving_glass: servingGlass,
      category: category
    });
    navigate("/cocktails");

    setCocktailName('');
    setAlcoholic(true);
    setIngredients('');
    setImage('');
    setInstructions('');
    setServingGlass('');
    setCategory('');
  };

  return (
    <div className="row" >
      <div className="offset-3 col-6" >
        <div className="shadow p-4 mt-4">
          <div>
          <div>
            <picture style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', marginBottom: '20px' }}>
              <source srcSet={require('./images/FinalCocktailCreate.png')} type="image/jpeg" />
              <img src={require('./images/FinalCocktailCreate.png')} alt="Cocktail" />
            </picture>
          </div>
          <form onSubmit={handleSubmit} id="create-cocktail-form">
            <div className="form-floating mb-3">
              <input
                onChange={handleCocktailNameChange}
                value={cocktailName}
                name="cocktailName"
                placeholder="Cocktail Name"
                required
                type="text"
                id="cocktailName"
                className="form-control"
              />
              <label htmlFor="cocktailName">Cocktail Name</label>
            </div>
            <div className="form-floating mb-3">
              <select
                value={alcoholic}
                onChange={handleAlcoholicChange}
                required
                type="text"
                name="alcoholic"
                id="alcoholic"
                className="form-control"
              >
                <option value="">Select Option</option>
                <option value="true">Alcoholic</option>
                <option value="false">Non-Alcoholic</option>
              </select>
              <label htmlFor="alcoholic">Alcoholic?</label>
            </div>
            <div className="form-floating mb-3">
              <input
                onChange={handleIngredientChange}
                value={ingredients}
                name="ingredients"
                placeholder="Ingredients"
                required
                type="text"
                id="ingredients"
                className="form-control"
              />
              <label htmlFor="ingredients">Ingredients</label>
            </div>
            <div className="form-floating mb-3">
              <input
                onChange={handleImageChange}
                value={image}
                name="image"
                placeholder="Image"
                required
                type="text"
                id="image"
                className="form-control"
              />
              <label htmlFor="image">Image</label>
            </div>
            <div className="form-floating mb-3">
              <input
                onChange={handleInstructionChange}
                value={instructions}
                name="instructions"
                placeholder="Instructions"
                required
                type="text"
                id="instructions"
                className="form-control"
              />
              <label htmlFor="instructions">Instructions</label>
            </div>
            <div className="form-floating mb-3">
              <input
                onChange={handleServingGlassChange}
                value={servingGlass}
                name="servingGlass"
                placeholder="Serving Glass"
                required
                type="text"
                id="servingGlass"
                className="form-control"
              />
              <label htmlFor="servingGlass">Serving Glass</label>
            </div>
            <div className="form-floating mb-3">
              <select
                value={category}
                onChange={handleCategoryChange}
                placeholder="category"
                required
                type="text"
                name="category"
                id="category"
                className="form-control"
              >
                <option value="">Choose a Category</option>
                <option value="ordinary drink">Ordinary Drink</option>
                <option value="cocktail">Cocktail</option>
                <option value="shake">Shake</option>
                <option value="shot">Shot</option>
                <option value="coffee or tea">Coffee or Tea</option>
                <option value="other">Other</option>
              </select>
              <label htmlFor="category">Category</label>
            </div>
            <button className="button">Create</button>
          </form>
          </div>
        </div>
      </div>
    </div>

  );
}

export default CocktailForm;
