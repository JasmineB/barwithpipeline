from fastapi.testclient import TestClient
from queries.ratings import RatingsQueries
from main import app

client = TestClient(app)


class FakeRatingsQueries:
    def get_cocktail_ratings(self, cocktail_id: str):
        return [
            {
                "review": "Worst drink ever2",
                "rating": 1,
                "id": "647653b838b13dc71718a473",
                "account_name": "Ray1",
                "account_id": "The Moon",
                "cocktail_id": cocktail_id,
            }
        ]


def test_get_cocktail_ratings():
    app.dependency_overrides[RatingsQueries] = FakeRatingsQueries
    response = client.get("/api/ratings/6476505138b13dc71718a470")

    data = response.json()

    assert data == {
        "ratings": [
            {
                "review": "Worst drink ever2",
                "rating": 1,
                "id": "647653b838b13dc71718a473",
                "account_name": "Ray1",
                "account_id": "The Moon",
                "cocktail_id": "6476505138b13dc71718a470",
            }
        ]
    }
